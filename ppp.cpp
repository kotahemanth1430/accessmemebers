#include<iostream>
using namespace std;
class class1{
    public:
    int pubvar;
    private:
    int privar;
    protected:
    int provar;
    public:
    void set(int pubvalue,int privalue,int provalue){
        pubvar=pubvalue;
        privar=privalue;
        provar=provalue;
    }
    void get()
    {
        cout<<"the value of public acces :"<<pubvar<<endl;
        cout<<"the value of private access:"<<privar<<endl;
        cout<<"the value of protected access: "<<provar<<endl;

    }

};
int main()
{
    class1 obj;
    obj.set(10,20,30);
    obj.get();
}